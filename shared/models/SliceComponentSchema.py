"""Slice Component model marshmallow schema"""
from marshmallow import Schema, fields, post_load
from SliceComponentDescriptor import SliceComponent
from VDUSchema import VDUSchema

class SliceComponentSchema(Schema):
  id = fields.Int(missing=None)
  vdu = fields.Nested(VDUSchema(), missing=None)

  @post_load
  def make_slice_component(self, data):
    return SliceComponent(**data)
