"""Slice model descriptor"""
from SliceComponentDescriptor import SliceComponent

class Slice:

  def __init__(self, id=None, components=[]):
    self.id = id
    self.components = components

  def add_component(self, component):
    self.components.append(component)

  def remove_component(self, component_id):
    for component in self.components:
      if component.id == component_id:
        remove = component
        break
    self.components.remove(component)
