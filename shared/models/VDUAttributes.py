"""Virtual Deployment Unit attributes dictionary keys"""
HOST = 'Host'
NAME = 'Name'
IMAGE = 'Image'
PUBLIC_PORT = 'Public_port'
PRIVATE_PORT = 'Private_port'
VIM = 'VIM'
WAN = 'WAN'
DOMAIN_ADDRESS = 'Domain_address'
DOMAIN_NAME = 'Domain_name'
