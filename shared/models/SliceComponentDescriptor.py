"""Slice Component model descriptor"""
from VDUDescriptor import VDU

class SliceComponent:

  def __init__(self, id=None, vdu_name=None, vdu_descriptor=None, vdu=None):
    self.id = id
    if vdu_descriptor is not None:
      self.vdu = VDU(id, vdu_descriptor['Name'], vdu_descriptor)
    elif vdu is not None:
      self.vdu = vdu
