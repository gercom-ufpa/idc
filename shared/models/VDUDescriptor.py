"""Slice Component's Virtual Deployment Unit descriptor"""
class VDU:

  def __init__(self, id=None, name=None, attributes={}, count=0):
    self.id = id
    self.name = name
    self.count = count
    self.attributes = attributes
    if 'Host' in attributes:
      self.count = len(attributes['Host'])
