"""Slice model marshmallow schema"""
from marshmallow import Schema, fields, post_load
from SliceDescriptor import Slice
from SliceComponentSchema import SliceComponentSchema

class SliceSchema(Schema):
  id = fields.Int(missing=None)
  components = fields.List(fields.Nested(SliceComponentSchema()), missing=[])

  @post_load
  def make_slice(self, data):
    return Slice(**data)
