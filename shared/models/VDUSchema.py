"""Slice Component's Virtual Deployment Unit model marshmallow schema"""
from marshmallow import Schema, fields, post_load
from VDUDescriptor import VDU

class VDUSchema(Schema):
  id = fields.Int(missing=None)
  name = fields.Str(missing=None)
  count = fields.Int(missing=0)
  attributes = fields.Dict(missing={})

  @post_load
  def make_vdu(self, data):
    return VDU(**data)
