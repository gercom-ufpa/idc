"""
Slice Controller
Responds to REST request with Docker access information (address, port, etc)
"""
def get_vim_access_info():
  # Reads VIM URL from conf file and returns it
  file = open('conf','r')
  text = file.read().splitlines()
  docker_url = text[0]
  return {"Url": docker_url}
