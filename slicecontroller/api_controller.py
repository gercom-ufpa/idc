"""Slice DC Controller"""
import hug
import SliceController

@hug.post()
def create_slice(body):
  """Create slice from specification and return resources handler"""
  # Currently, it only returns the VIM(Docker) URL
  vim_access_info = SliceController.get_vim_access_info()
  return vim_access_info
