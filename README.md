## Itinerante Data Center

The IDC is the Fog Computing concept [Mouradian 2018] applied as a service. The main idea is to have an itinerant data center that could host microservices on demand, allowing the service coverage of islands without Internet access, thus,  islands that normally did not reach the core network to access the services. These services (microservices) need a slice allocation, deployment of the service over the slice and the migration/replication of the service in the itinerant data center equipment.

The Service Provider must request the slice creation, which is performed by the Slice Provider using the Slice Resource Orchestrator. The Service Provider must insert the service definition to be handled by the Slice Provider in order to receive the slice definition used to select the resources.


## Environment

Now we will prepare the environment that will be used in the application.

1.  Make six VM's
2.  Install pip3 in all machines
3.  Set IDC-Core, IDC-Boat, IDC-S.Provider, IDC-WanController, 
4.  Install [*Docker*](https://docs.docker.com/install/linux/docker-ce/debian/#install-using-the-repository) on IDC-Core e IDC-Boat
5.  Install [*Hug*](http://www.hug.rest/website/quickstart) on IDC-Core, IDC-Boat, IDC-S.Provider, IDC-WanController
6.  Install [*Marshmallow*](https://marshmallow.readthedocs.io/en/3.0/) on IDC-Core, IDC-Boat, IDC-S.Provider, IDC-WanController
7.  Install *dnsmasq* on IDC-WanController
8.  Open a port for the IDC-Core and IDC-Boat Docker to be remotely controlled, creating the file */etc/systemd/system/docker.service.d/override.conf* and edit
>       [Service]
>       ExecStart=
>       ExecStart=/usr/bin/dockerd -H unix:///var/run/docker.sock -H fd:// -H tcp://0.0.0.0:2375`
restart these vms
        
9.  Create a virtual network between all machines

    **IDC-CORE and IDC-Boat**
    
10. Add on IDC-Core and IDC-Boat the folders *slicecontroller* and *shared*
11. Change the *conf* file in the *slicecontroller* folder to match the reachable address of the Docker API
12. Add on IDC-Core the file *service.tar*

    **IDC-S.Provider**
13. Add on IDC-S.Provider the folders *sliceprovider* and *shared*

    **IDC-WanController**
14. Add on IDC-WanController the folders *wancontroller* and *shared*

    **Client**
15. Configure the client's nameserver to include the IDC-WanController

## Start

With the environment properly configured, we can start the service

1.  Clear any trace of containers on IDC-Core and IDC-Boat with the commands 
>       sudo docker system prune -a

2. Load the service image on the IDC-Core with the command
>       sudo docker load -i service.tar

3.  Now execute all api's in machines with the command 
>       sudo hug -f name_api.py

4. On the client vm, run the *client.py* in the *client* folder to send requests to the Slice Provider