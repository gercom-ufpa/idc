"""
Slice WAN Controller
Inserts/Alters the name:address tuple from simple DNS server hosts file
"""
from subprocess import run

def update_dns_server(address, name):
  # Reads /etc/hosts file
  file = open('/etc/hosts','r')
  text = file.read()
  # If the domain's name is present, update it
  if name in text:
    lines = text.splitlines()
    file.close()
    file = open('/etc/hosts', 'w')
    updated_text = ''
    for line in lines:
      if name in line:
        line = address+'\t'+name
      updated_text += line+'\n'
    file.write(updated_text)
  # If the domain's name is not present, insert it
  else:
    file = open('/etc/hosts', 'a')
    file.write(address+'\t'+name+'\n')
  # Close the file
  file.close()
  # Reset the dnsmasq service
  run(['/etc/init.d/dnsmasq', 'restart'], check=True)
