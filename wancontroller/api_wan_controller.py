"""Slice WAN Controller"""
import hug
import sys
sys.path.append('../shared/models')
from SliceSchema import SliceSchema
import SliceWANController

@hug.post()
def create_slice(body):
  "Creates slice from specifications"
  # Loads specification from message body
  slice = SliceSchema().loads(body)
  # Obtains domain's address and name
  address = slice.components[0].vdu.attributes['Domain_address']
  name = slice.components[0].vdu.attributes['Domain_name']
  # Updates the DNS server address/name
  SliceWANController.update_dns_server(address, name)
  return True
