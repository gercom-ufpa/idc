"""
Slice Resource Orchestrator
Manages the process of obtaining access information from Itinerant DC and replicating the container
"""
import sys
sys.path.append('../shared/models')
import VDUAttributes
from SliceSchema import SliceSchema
import ima.ResourceManagement as ResourceManagement
import SliceBuilder
import db.SliceGraph as SliceGraph
from interactor.SliceControllerInteractor import SliceControllerInteractor

# Receives the update operation and forward the slice specification to the appropriate handler method
def handle_update(operation, slice_definition):
  # Retrieves the VIM URL for the specified slice components
  SliceBuilder.retrieve_vim_access_information(slice_definition)
  # Retrieve the name and WAN URL attributes from the component to be updated
  for component in slice_definition.components:
    component.vdu.attributes[VDUAttributes.NAME] = SliceGraph.get_slice_component_vdu_attribute(slice_definition.id, component.id, VDUAttributes.NAME)
    component.vdu.attributes[VDUAttributes.WAN] = SliceGraph.get_slice_component_vdu_attribute(slice_definition.id, component.id, VDUAttributes.WAN)
  # Forward slice specification to the appropriate handler method
  if operation == 'ADD_REPLICA':
    replicate_virtual_resource(slice_definition)
  elif operation == 'REMOVE_REPLICA':
    release_virtual_resource(slice_definition)
  # Return the slices current state
  return SliceGraph.get_slices()

# Instantiate the virtual resources as specified by the new slice
def instantiate_slice(slice_definition):
  # For every component's VIM, instantiate the virtual resource as specified
  for component in slice_definition.components:
    for vim_url in component.vdu.attributes[VDUAttributes.VIM]:
      # Obtain a VIM Wrapper to send commands to the VIM
      vimw = ResourceManagement.get_vim_wrapper({'Url': vim_url})
      # Create and run a virtual resource as specified by the VDU attributes
      container = vimw.run_virtual_resource(component.vdu.attributes)
    # Update the domain name:address tuple
    update_domain_address(component.vdu.attributes[VDUAttributes.WAN], slice_definition)
  # Add the slice information to the slice graph
  SliceGraph.add_slice(slice_definition)
  # Return the slices current state
  return SliceGraph.get_slices()

# Create a replica of the specified virtual resource
def replicate_virtual_resource(slice_definition):
  for component in slice_definition.components:
    # Retrieve the VIM URLs from the slice graph
    vim_urls = SliceGraph.get_slice_component_vdu_attribute(slice_definition.id, component.id, VDUAttributes.VIM)
    # Use the first URL as source for the replica
    src_vim_url = vim_urls[0]
    # Obtain a VIM Wrapper for the source VIM
    src_vimw = ResourceManagement.get_vim_wrapper({'Url': src_vim_url})
    # Create a replica for each of the target VIMs
    for vim in component.vdu.attributes[VDUAttributes.VIM]:
      # If VIM URL is already on the list, it has a replica already
      if vim in vim_urls:
        continue
      # Create a replica of the specified virtual resource from the source VIM to the destination VIM
      container = src_vimw.replicate_virtual_resource(container_id=component.vdu.attributes[VDUAttributes.NAME], dest_url=vim)
    # Update the domain's name:address tuple as specified
    update_domain_address(component.vdu.attributes[VDUAttributes.WAN], slice_definition)
  # Add the replica information to the slice graph
  SliceGraph.add_replica(slice_definition)

# Stop and remove an specified virtual resource
def release_virtual_resource(slice_definition):
  for component in slice_definition.components:
    # Retrieve the hosts URLs from the slice graph
    host_urls = SliceGraph.get_slice_component_vdu_attribute(slice_definition.id, component.id, VDUAttributes.HOST)
    # Retrieve the VIM URLs from the slice graph
    vim_urls = SliceGraph.get_slice_component_vdu_attribute(slice_definition.id, component.id, VDUAttributes.VIM)
    # Remove the virtual resource from every host specified in the slice definition
    for host in component.vdu.attributes[VDUAttributes.HOST]:
      # Retrieve the corresponding VIM URL from the host
      vim_url = vim_urls[host_urls.index(host)]
      # Obtain a VIM Wrapper to send commands to the VIM
      vimw = ResourceManagement.get_vim_wrapper({'Url': vim_url})
      # Update all replicas of the virtual resource with the data of the to-be-removed replica
      for vim in vim_urls:
        # If the vim is from the replica to be removed, skip it
        if vim == vim_url:
          continue
        else:
          # Remove the virtual resource replica (to replace it with a updated replica)
          ResourceManagement.get_vim_wrapper({'Url': vim}).remove_virtual_resource(component.vdu.attributes[VDUAttributes.NAME])
          # Replicate the virtual resource that is going to be removed, updating the other replicas
          vimw.replicate_virtual_resource(component.vdu.attributes[VDUAttributes.NAME], vim)
      # Finally remove the virtual resource targeted for removal
      vimw.remove_virtual_resource(component.vdu.attributes[VDUAttributes.NAME])
    # Update the domain's name:address tuple as specified
    update_domain_address(component.vdu.attributes[VDUAttributes.WAN], slice_definition)
  # Remove the replica information from the slice graph
  SliceGraph.remove_replica(slice_definition)

# Send a request to the Slice WAN Controller to update the domain's name:address tuple
def update_domain_address(wan_url, slice_definition):
  SliceControllerInteractor(info={'Url': wan_url}).send_request("create_slice", SliceSchema().dumps(slice_definition))
