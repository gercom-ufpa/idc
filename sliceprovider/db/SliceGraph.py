"""Slice Graph data access object"""
import sys
sys.path.append('../../shared/models')
import VDUAttributes

# Keep the slices in memory (for now)
slices = []

# Add a slice to slices
def add_slice(slice_definition):
  # Set the component's ids
  slice_definition.id = len(slices)
  for component in slice_definition.components:
    component.id = slice_definition.components.index(component)
    component.vdu.id = component.id
  # Store the specified slice
  slices.append(slice_definition)
  return slice_definition

# Add a replica to a slice component
def add_replica(slice_definition):
  # For every specified component, update the pertinent fields
  for component in slice_definition.components:
    # Recover the corresponding slice component from storage
    og_component = get_slice_component(slice_definition.id, component.id)
    # Add the new replica host URL to the host list
    og_component.vdu.attributes[VDUAttributes.HOST] += component.vdu.attributes[VDUAttributes.HOST]
    # Add the new replica VIM URL to the VIM list
    og_component.vdu.attributes[VDUAttributes.VIM] += component.vdu.attributes[VDUAttributes.VIM]
    # Update the domain name
    og_component.vdu.attributes[VDUAttributes.DOMAIN_NAME] = component.vdu.attributes[VDUAttributes.DOMAIN_NAME]
    # Update the domain address
    og_component.vdu.attributes[VDUAttributes.DOMAIN_ADDRESS] = component.vdu.attributes[VDUAttributes.DOMAIN_ADDRESS]
    # Update count
    og_component.vdu.count += 1

# Remove a replica from a slice component
def remove_replica(slice_definition):
  # For every specified component, update the pertinent fields
  for component in slice_definition.components:
    # Recover the corresponding slice component from storage
    og_component = get_slice_component(slice_definition.id, component.id)
    # Remove the replicas hosts URL from the host list
    for host in component.vdu.attributes[VDUAttributes.HOST]:
      try:
        og_component.vdu.attributes[VDUAttributes.HOST].remove(host)
      except ValueError:
        pass
    # Remove the replicas VIM URL from the VIM list
    for vim in component.vdu.attributes[VDUAttributes.VIM]:
      try:
        og_component.vdu.attributes[VDUAttributes.VIM].remove(vim)
      except ValueError:
        pass
    # Update the domain name
    og_component.vdu.attributes[VDUAttributes.DOMAIN_NAME] = component.vdu.attributes[VDUAttributes.DOMAIN_NAME]
    # Update the domain address
    og_component.vdu.attributes[VDUAttributes.DOMAIN_ADDRESS] = component.vdu.attributes[VDUAttributes.DOMAIN_ADDRESS]
    # Update count
    og_component.vdu.count -= 1

# Getter to obtain a VDU attribute from a specific slice component
def get_slice_component_vdu_attribute(slice_id, component_id, attribute):
  return slices[slice_id].components[component_id].vdu.attributes[attribute]

# Getter to obtain slices
def get_slices():
  return slices

# Getter to obtain a slice
def get_slice(slice_id):
  for slice in slices:
    if slice.id == slice_id:
      return slice

# Getter to obtain a slice component
def get_slice_component(slice_id, component_id):
  slice = get_slice(slice_id)
  for component in slice.components:
    if component.id == component_id:
      return component
