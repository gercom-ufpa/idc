"""
Res. & VM Mgmt. component of the Infrastructure Management & Abstraction (IMA)
Obtains appropriate wrapper for a VIM to manage its resources
"""
from ima.DockerWrapper import DockerWrapper

# Dependency injection method to obtain a wrapper for Docker API
def get_vim_wrapper(info):
  return DockerWrapper.factory(info=info)
