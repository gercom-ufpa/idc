"""
DockerWrapper component of the Infrastructure Management & Abstraction (IMA)
Interacts with Docker to replicate a container from origin to destiny
"""
from docker import APIClient

# Wrapper for Docker API calls
class DockerWrapper:

  def __init__(self, info):
    self.client = APIClient(base_url=info['Url'], tls=False)

  @classmethod
  def factory(cls, info):
    return cls(info)

  # Instantiates a container according to slice specifications
  def run_virtual_resource(self, slice_definition):
    image_name = slice_definition['Image']
    container_name = slice_definition['Name']
    private_port = slice_definition['Private_port']
    public_port = slice_definition['Public_port']
    # Create a container as specified
    container = self.client.create_container(image_name,
      name=container_name, ports=[private_port],
      host_config=self.client.create_host_config(
      port_bindings={private_port: public_port}))
    # Run the created container
    self.client.start(container)
    return container

  # Replicates live container image to client in destination url
  def replicate_virtual_resource(self, container_id, dest_url, tag='latest'):
    orig_client = self.client
    dest_client = APIClient(base_url=dest_url, tls=False)

    # Obtain the info about the container to be replicated
    container = self.__get_container_info(container_id)
    container_name = container['Names'][0]
    image_name = container['Image']
    if image_name.endswith(':'+tag):
      image_name = image_name[:-(len(tag)+1)]
    private_port = container['Ports'][0]['PrivatePort']
    public_port = container['Ports'][0]['PublicPort']

    # If container instance is diferent from its image (files have been modified)
    if orig_client.diff(container['Id']) is not None:
      # Commit the container to the image, updating it
      orig_client.commit(container['Id'], repository=image_name, tag=tag)

    image = orig_client.get_image(image_name+':'+tag)

    # Transfer the image
    dest_client.load_image(image)
    # Create a identical container on destiny server
    replica = dest_client.create_container(image_name,
      name=container_name, ports=[private_port],
      host_config=dest_client.create_host_config(
      port_bindings={private_port: public_port}))
    # Run the created container
    dest_client.start(replica)
    return replica

  # Stops and removes container and associated image
  def remove_virtual_resource(self, container, purge=False):
    # Obtain the container info
    info = self.__get_container_info(container)
    # Stop the container
    self.client.stop(info['Id'])
    # Remove it from server
    self.client.remove_container(info['Id'])
    # If purge is True, remove image as well
    if purge:
      self.client.remove_image(info['Image'])

  # Private method to obtain container attributes from id or name
  def __get_container_info(self, container):
    info = self.client.containers(filters={"id": container})
    if len(info) > 0:
      return info[0]
    info = self.client.containers(filters={"name": container})
    if len(info) > 0:
      return info[0]
