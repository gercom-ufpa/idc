"""
SliceControllerInteractor is an abstraction to send/receive requests from a
slice dc/wan controller remotely
"""
import requests

class SliceControllerInteractor:

  # Receives the Slice Controller URL
  def __init__(self, info):
    self.url = info['Url']

  # Sends a POST message to the specified method call with the given parameters
  def send_request(self, method, parameters):
    return requests.post(self.url+'/'+method, parameters)
