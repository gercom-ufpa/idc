"""
Slice Builder
Sends request to Slice Controller REST API to obtain necessary access information of the Itinerant DC Docker's instance
"""
import json
import sys
sys.path.append('../shared/models')
import ResourceOrchestrator
from SliceSchema import SliceSchema
import VDUAttributes
from interactor.SliceControllerInteractor import SliceControllerInteractor

# Completes the slice model and forward it to the ResourceOrchestrator
def create_slice_instance(slice_definition):
  # Obtain the VIM URL for the components of the specified slice
  retrieve_vim_access_information(slice_definition)
  # Forwards the updated slice definition to the ResourceOrchestrator
  return ResourceOrchestrator.instantiate_slice(slice_definition)

# Contacts the Slice Controllers to obtain the VIM URL for the slice
def retrieve_vim_access_information(slice_definition):
  for component in slice_definition.components:
    # If the slice definition doesn't have a VIM key, instantiate it
    if VDUAttributes.VIM not in component.vdu.attributes:
      component.vdu.attributes[VDUAttributes.VIM] = []
    # For every Slice Controller host URL, obtain the corresponding VIM URL
    for host in component.vdu.attributes[VDUAttributes.HOST]:
      # Instantiate interactor with the Slice Controller host URL
      interactor = SliceControllerInteractor(info={'Url': host})
      # Use the interactor to send a request
      response = interactor.send_request("create_slice", SliceSchema().dumps(slice_definition))
      # Decode the answer
      info = json.loads(response.content.decode("utf-8"))
      # Append the URL to the component's VDU VIM attribute
      component.vdu.attributes[VDUAttributes.VIM].append(info['Url'])
