"""LSDC API to create and update slices"""
import hug
import sys
sys.path.append('../../shared/models')
sys.path.append('../')
from SliceSchema import SliceSchema
import ResourceOrchestrator
import SliceBuilder
import json

@hug.post()
def request_slice(body):
  """Creates a new slice from slice specifications"""
  # Loads the slice object from the json codified body
  slice = SliceSchema().loads(body)
  # Sends to SliceBuilder to instantiate the slice
  slices = SliceBuilder.create_slice_instance(slice)
  # Returns the current state of the slice graph
  return SliceSchema().dumps(slices, many=True)

@hug.post()
def update_slice(body):
  """Updates an existing slice by receiving an operation and the update specifications"""
  # Loads the json into a python dictionary
  data = json.loads(body)
  # Retrieves the operation
  op = data['Operation']
  # Retrieves the slice specification
  slice = SliceSchema().loads(data['Slice'])
  # Sends to ResourceOrchestrator to handle the update operation
  slices = ResourceOrchestrator.handle_update(op, slice)
  # Returns the current state of the slice graph
  return SliceSchema().dumps(slices, many=True)
