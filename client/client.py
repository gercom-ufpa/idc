"""Client for the LSDC"""
import sys
sys.path.append('../shared/models')
from SliceDescriptor import Slice
from SliceSchema import SliceSchema
import readline, requests, json

# Default slice request json specification
slice_request = '{"components": [{"vdu": {"name": "vdu", "attributes": {"Private_port": "80", "Name": "idc", "Public_port": "4000", "Host": ["http://X.X.X.X:8000"], "Image": "service", "Domain_address": "X.X.X.X", "Domain_name": "service.gercom", "WAN": "http://X.X.X.X:8000"}}}]}'
# Default slice update json specification
slice_update = '{"components": [{"vdu": {"attributes": {"Host": ["http://X.X.X.X:8000"], "Domain_address": "X.X.X.X", "Domain_name": "service.gercom"}, "id": 0}, "id": 0}], "id": 0}'

# Allows for an user input with a default text already inputed
def input_with_prefill(prompt, text):
  def hook():
    readline.insert_text(text)
    readline.redisplay()
  readline.set_pre_input_hook(hook)
  result = input(prompt)
  readline.set_pre_input_hook()
  return result

# Obtain the LSDC URL
lsdc_url = input_with_prefill('LSDC URL: ', 'http://X.X.X.X:8000')
operation = 0
while operation != '4':
  operation = input('\n\nChoose operation: (1)Request slice (2)Add Replica (3)Remove replica (4)Exit: ')
  if operation == '1':
    slice_request = input_with_prefill('\nSlice definition: ', slice_request)
    slice = SliceSchema().loads(slice_request)
    data = SliceSchema().dumps(slice)
    result = requests.post(lsdc_url+'/request_slice', data)
    print('\n'+result.text)
  elif operation == '2':
    slice_update = input_with_prefill('\nSlice definition: ', slice_update)
    slice = SliceSchema().loads(slice_update)
    data = SliceSchema().dumps(slice)
    result = requests.post(lsdc_url+'/update_slice', json.dumps({'Operation': 'ADD_REPLICA', 'Slice': data}))
    print('\n'+result.text)
  elif operation == '3':
    slice_update = input_with_prefill('\nSlice definition: ', slice_update)
    slice = SliceSchema().loads(slice_update)
    data = SliceSchema().dumps(slice)
    result = requests.post(lsdc_url+'/update_slice', json.dumps({'Operation': 'REMOVE_REPLICA', 'Slice': data}))
    print('\n'+result.text)
  elif operation != '4':
    print('\n'+operation+' is an invalid input!')
